# Redmine_time_entries

At work I do one occurring task every Monday. It consists of getting data from Redmine from last week, compile it into a csv file and send it to my other colleague. I have decided to automate it using Python script.

## What does script do?

- Connects to company's Redmine via API key,
- filters time entries from last week,
- exports it into csv file,
- edits the csv into user report,
- sends user report via email to desired recipients.

## What to know before running the script?

**You need to set up environmental variables on your computer** because the script works with them as variables. If you do not know how to do this, you have two options - either you hard-code your information to the script (not recommended) or you learn how to set up environmental variables on your machine (Google is your friend).

You also need to **set up a Python environment** for running the script. File *requirements.txt* is in the repository. Do something like this:

```
python3 -m venv /path/to/new_virtual_environment
source /path/to/new_virtual_environment/bin/activate
pip install -r requirements.txt
```

Also, **change your smtp server on line 94 in the script**. I send data from my work email, but you probably want to send data from your own personal mailbox (smtp server needs to be changed then).

## Beware: The script creates new files

Just to be aware, the script, as it was thougth to be applied, creates two csv files (one unformatted and one with desired format) and one log file where logging events are stored. These files are saved in *source* folder.

## What did I learn from writing the script?

- How to handle environmental variables on my Linux machine,
- how to log events in my script,
- how to send email from Python script,
- how to use datetime package.

It is a great feeling of accomplishment when you are able to force computer do your assigned task.
