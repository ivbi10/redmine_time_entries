# -*- coding: utf-8 -*-

import os
from datetime import date, timedelta
from redminelib import Redmine

# set environmental variables used for authentification to services
API_KEY = os.environ.get("REDMINE_API_KEY")

# connect to Redmine
try:
    redmine = Redmine('https://redmine.proofreason.com',
                      key=API_KEY)
except:
    raise Exception("Connection to Redmine failed.")

# set datetime for last Monday
today = date.today()
last_monday = today - timedelta(days=today.weekday()) - timedelta(days=7)
last_sunday = last_monday + timedelta(days=6)


def get_time_entries():
    """
    The function filters time entries from the last week
    and saves it into a csv file.

    Output csv file is stored in data folder.
    """
    time_entries_lastweek = redmine.time_entry.filter(
        from_date=last_monday, to_date=last_sunday)
    time_entries_lastweek.export('csv', savepath='.', filename='entries.csv')


if __name__ == '__main__':
    get_time_entries()
