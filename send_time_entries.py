# -*- coding: utf-8 -*-

import os
import pandas as pd
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


# set environmental variables used for authentification to services
EMAIL_ADDRESS = os.environ.get("EMAIL_USER")
EMAIL_PASSWORD = os.environ.get("EMAIL_PASS")


def process_user_hours():
    """
    Function filters export from Redmine and creates a summary
    of hours by user.
    
    Output csv file is saved in working directory.
    """
    
    df = pd.read_csv('entries.csv')
    filter_data = df.loc[:, ['Uživatel', 'Hodiny']]
    result = filter_data.groupby('Uživatel').sum()

    # round second column (Hodiny) on two decimal points
    result.Hodiny = result.Hodiny.round(2)

    # export into csv
    result.to_csv('user_hours.csv', sep=';', decimal=',')


def find_client(project):
    """
    Helper function to identify client project.

    Returns 0 if project is internal.
    Returns 1 if project is for client.
    """
    
    if "90" in project:
        return 0
    else:
        return 1


def client_hours():
    """
    Function filters export from Redmine and creates a summary
    of hours by projects that are either internal or client ones.
    
    It uses a helper function that classifies projects in two
    categories - internal and client project.
    
    Output csv file is saved in working directory.
    """
    
    data = pd.read_csv('entries.csv')
    data = data.loc[:, ["Projekt", "Hodiny"]]
    data["is_client"] = data["Projekt"].apply(find_client)
    data = data.groupby('is_client')[["Hodiny"]].agg('sum')
    data["Hodiny"] = data["Hodiny"].round(2)

    # export into csv
    data.to_csv('client_hours.csv', sep=';', decimal=',')


files = ["user_hours.csv", "client_hours.csv"]

recipients = ["pm@proofreason.com", EMAIL_ADDRESS]

def send_csv_via_email():
    """
    Function uses built-in library to send email to a specified
    e-mail address. 
    
    E-mail address is stored in environment variable. 
    It can be edited in the code below.
    """
    
    msg = MIMEMultipart()
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = ", ".join(recipients)
    msg['Subject'] = 'REPORT: Čas z Redmine za minulý týden'

    body = """\
    V prílohe sú súbory pre účely plánovacej porady. Toto je automatický e-mail.
    
    Ivan
    """

    msg.attach(MIMEText(body, 'plain'))

    for file in files:
        filename = file

        with open(filename, 'rb') as f:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(f.read())

        encoders.encode_base64(part)
        part.add_header("Content-Disposition", "attachment", filename=filename)

        msg.attach(part)

    with smtplib.SMTP_SSL('smtp.web4u.cz', 465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.send_message(msg)
        print("Email sent.")


if __name__ == '__main__':
    process_user_hours()
    client_hours()
    send_csv_via_email()
